let students = ["Linda", "Dave", "Melissa"]

// adds a new value on the last index
students.push("John")

// removes value from the last index and returns it
let popped = students.pop()

let i = 0
while(i < students.length){
    // console.log(students[i] + " passed the test!")
    i++
}

for(let i = 0; i < students.length; i++){
    // console.log(students[i] + " passed the test!")
}

for(let i in students){
    // console.log(students[i] + " passed the test!")
}

for(let s of students){
    // console.log(s + " passed the test!")
}

students.forEach((student) => {
    // console.log(student + " passed the test!")
})

/**
 * Sorting arrays
 */

students.sort()
// console.log(students)

const studentsGrades = [
    {name: "Melissa", grade: 95},
    {name: "Dave", grade: 70},
    {name: "Linda", grade: 100}
]

studentsGrades.sort()
// console.log(studentsGrades)

studentsGrades.sort(mySort)

function mySort(a, b){
    if(a.grade < b.grade)
        return 1
    if(a.grade > b.grade)
        return -1
    return 0
}

console.log(studentsGrades)