function a(){
    return "a"
}

function b(){
    return "b"
}

a()
b()

setTimeout(() => {
    // console.log("Hello")
}, 3000)

// console.log("World")

const promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('foo')
    }, 3000)
})

console.log(promise1)

promise1
    .then(value => {
        console.log(value)
    })

const promise2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        reject(new Error('An error occurred'))
    }, 3000)
})

promise2
    .catch(error => {
    console.log(error.message)
})

/**
 * Chaining promises
 */

const visitATM = (money) => {
    return money
}
const playSlots = (money) => {
    return money / 2
}

const playPoker = (money) => {
    return money - 200
}

const goToBar = () => {}

let withdraw = new Promise(function(resolve,reject){

    let amount = visitATM(1000);

    return resolve(amount)
});

withdraw
    .then(function(amount){
        let slotResults = playSlots(amount);

        if(slotResults <= 0)
            throw Error;

        return slotResults;
    })
    .then(function(slotResults){
        let pokerResults = playPoker(slotResults);

        if(pokerResults <= 0)
            throw Error;

        return pokerResults;
    })
    .catch(function(e){
        goToBar();
    });

/**
 * async/await usage
 */

const shower = (time) => new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(time + 3000)
    }, 3000)
})

const eatBreakfast = (time) => new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(time + 3000)
    }, 3000)
})

const morningRoutine = async (startTime) => {
    try {
        const step1FinishTime = await shower(startTime)
        const step2FinishTime = await eatBreakfast(step1FinishTime)
        return step2FinishTime;
    }
    catch (e) {
        return e;
    }
};

morningRoutine(0)
    .then((finishTime) =>
        finishTime += " ms"
        // console.log(`Routine took ${finishTime}`)
    )
    .catch(e => console.log(e))