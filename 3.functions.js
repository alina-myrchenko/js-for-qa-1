function addVars(a, b){
    let sum = a + b
    return sum
    // return a + b
}

let result = addVars(3, 4)
console.log(result)

function average(a, b){
    let result = addVars(a, b) / 2
    return result
}

result = average(10, 20)
console.log(result)

const subtractVars = function (a, b){
    return a - b
}

result = subtractVars(20, 10)
console.log(result)


let multiplyVars = (a, b) => {
    return a * b
}

console.log(multiplyVars(3, 4))

/**
 * Scope of variables
 */

function getDiscount(sum){
    const discount = 0.05
    if(sum >= 1000){
        const discount = 0.1
        let newSum = sum * discount
        return newSum
    }
    else {
        return sum * discount
    }
    console.log(newSum)
}
getDiscount(1200)
// console.log(discount)