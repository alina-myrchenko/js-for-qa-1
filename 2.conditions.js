// // conditions
// let age = 5
// let gender = 'female'
//
// if(age === 0){
//     console.log('age cannot be 0!')
// }else if(age >= 18){
//     console.log("Adult")
// }else if(age >= 12){
//     console.log("Teenager")
// }else if(age > 0 && gender === 'female'){
//     console.log("Girl")
// }else if(age > 0 && gender === 'male'){
//     console.log("Boy")
// }else{
//     console.log('invalid data!')
// }

// switch case
let browser = 'Safari'
switch (browser) {
    case 'Edge':
        console.log("You've got the Edge!");
        break;

    case 'Chrome':
    case 'Firefox':
    case 'Safari':
    case 'Opera':
        console.log('Okay we support these browsers too');
        break;

    default:
        console.log('We hope that this page looks ok!');
}