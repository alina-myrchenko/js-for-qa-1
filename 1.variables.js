// var and let are for both write and read
var x

x = true

x = "Hello world"
// console.log(typeof(x))

// let is a local variable
let count = 10

if(count > 5){
    let newCount = 5
}

// const is read-only
const user = {
    name: "John",
    age: 50,
    gender: null
}
console.log(user.name)

/**
 * Strings declarations
 */
let firstName = 'John'
let lastName = "Snow"
let fullName = `Name: ${firstName} ${lastName}`
// console.log(fullName)